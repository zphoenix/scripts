import requests
import json
from datetime import datetime

spanoUrl = "http://10.56.130.10:5000/"
baseUrl="http://10.56.130.10:9002/recordings/"
deviceName="Radio Pi"
currentTime=(datetime.now().strftime("%d/%m/%Y-%H:%M:%S"))
name=(deviceName + "-" + currentTime)
description="Uploaded from " + deviceName
location="Halekote, Tumkur"
skill="Recording DurgadahalliRadio " +deviceName
tags="Recording DurgadahalliRadio " + deviceName

spanoData = open(r'/home/z/Janastu/scripts/demo.mp3', 'rb')  
files = {'file' : spanoData} 
res1 = requests.post(spanoUrl, files=files)    

response = res1.text.split('"').pop(-2).split('/').pop(-1)
type = response.split('.').pop(-1)

baseData = {"name": (deviceName + "-" + currentTime), "contentUrl": response, "embedUrl": "", "description": description, "station_name": deviceName, "created": currentTime, "type": type, "author": deviceName + "'s User", "location": location, "skill": skill, "tags": tags }
res2 = requests.post(baseUrl, json=baseData)
  